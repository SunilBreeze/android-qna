About the app
-
The purpose of this application is to provide and interface to its user, 
where in the user is can ask questions, in natural english language
and receive appropriate replies. The android frontend interacts with 
Microsoft QnA Maker, over RESTApi, android makes these REST requests via [Retrofit](http://square.github.io/retrofit/) android networking library.

Microsoft QnA Maker
-
The [Microsoft QnA Maker](https://qnamaker.ai/) is a text processing product, developed by Microsoft
the product is free upto a limit of **10000, request's per month.**
Know more about Microsoft QnA Maker[here](https://qnamaker.ai/Documentation/Faqs).
 
